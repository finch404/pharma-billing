import React, { Component } from 'react'
import './inventory.css'
import { connect } from 'react-redux';
import axios from 'axios';
import { postNewProduct } from "../../../store/actions/Product/product";
import { Whatsapp } from '../../Home/Whatsapp';
import { fetchTotalProduct } from "../../../store/actions/Product/product";
import { fetchTotalVendors } from "../../../store/actions/Vendors/vendors";
import SimpleReactValidator from "simple-react-validator";
import { API_URL } from '../../../url/url'
export class AddInventoryItem extends Component {
  locale = 'en-IN'
  currency = 'INR'
  constructor(props) {
    super(props);
    // React state
    this.state = {
      ui_vendor_name: '',
      ui_vendor_mobile: '',
      ui_vendor_gstin: '',
      ui_vendor_email: '',
      item_id: "",
      item_inventory_quantity: '',
      item_batch: '',
      item_batch_expiry: '',
      item_sgst_percentage: '',
      item_cgst_percentage: '',
      item_igst_percentage: '',
      item_total_gst_amount: '',
      item_price: '',
      ui_items: [{
        item_cgcst_amount: '',
        item_sgst_amount: '',
        item_igst_amount: '',
        item_price: '',
        item_name: '',
        item_id: "",
        item_inventory_quantity: '',
        item_batch: '',
        item_batch_expiry: '',
        item_sgst_percentage: '',
        item_cgst_percentage: '',
        item_igst_percentage: '',
        item_total_gst_amount: '',
        item_tax_percentage : '',
        item_subtotal : '',
        item_total : ''
      }],
      cgstAmount: '',
      sgstAmount: '',
      igstAmount: '',
      gstAmount: ''


    }; this.validator = new SimpleReactValidator();
  }
  componentDidMount() {
    this.props.fetchTotalProduct(this.props);
    this.props.fetchTotalVendors(this.props);

  }
  // by using arrow functions, we retain value of 'this', and don't need to bind 'this' in the constructor
  handleChange = e => {
    this.setState({
      // get computed property name and set its matching value
      // (works only if the name matches what's in state! but is a nice way to generalize form changes on different types of inputs)
      [e.target.name]: e.target.value
    });
    this.formatCurrency(this.calcCgstAmount())
    this.formatCurrency(this.calcGstAmount())

  };
  vendorclick = async e => {
    // console.log(this.state.name)
    // const ableToDrink = this.props.vendors.filter( age => age.id > -1 );
    // console.log(ableToDrink[0].id)
    // let newfilteredarr = []
    // let newfilteredarr = this.props.product.filter(products => products.id == this.state.item_id);
    // console.clear()
    // console.log(newfilteredarr[0].id)
    for (let i = 0; i < this.props.vendors.length; ++i) {
      if (this.props.vendors[i].id == this.state.name) {
        console.clear()
        console.log(this.props.vendors[i])


        await this.setState({
          ui_vendor_name: this.props.vendors[i].name,
          ui_vendor_mobile: this.props.vendors[i].mobile,
          ui_vendor_gstin: this.props.vendors[i].gstin,
          ui_vendor_email: this.props.vendors[i].email,
        })

      }
    }

  }
  siteclick = async e => {
    // let newfilteredarr = []
    // let newfilteredarr = this.props.product.filter(products => products.id == this.state.item_id);
    // console.clear()
    // console.log(newfilteredarr[0].id)
    for (let i = 0; i < this.props.product.length; ++i) {
      if (this.props.product[i].id == this.state.item_id) {
        console.clear()
        console.log(this.props.product[i])

        // this.state.item_price = this.props.product[i].sellingPrice
        await this.setState({
          item_price: this.props.product[i].sellingPrice || '0',
          item_sgst_percentage: this.props.product[i].sgst || '0',
          item_cgst_percentage: this.props.product[i].cgst || '0',
          item_igst_percentage: this.props.product[i].igst || '0',
        })

      }
    }
    console.log(this.state.item_price)
    this.formatCurrency(this.calcCgstAmount())
    this.formatCurrency(this.calcGstAmount())

    //  }
    // // this.state.newitemarr = ableToDrink;
    // await this.setState({
    //   newitemarr : ableToDrink
    // })
    // console.log(this.props.product[0].sellingPrice)
    // this.setState({
    //   item_price : ableToDrink.sellingPrice
    // })
    // console.log(this.state.item_id);

  }

  formatCurrency = (amount) => {
    return (new Intl.NumberFormat(this.locale, {
      // style: 'currency',
      // currency: this.currency,
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    }).format(amount))
  }

  calcGstAmount() {
    let gstAmount = +this.state.cgstAmount + +this.state.sgstAmount + +this.state.igstAmount
    this.setState({
      item_total_gst_amount: gstAmount
    })
  }

  change = (idx) => (e) => {
    const newShareholders = this.state.ui_items.map((shareholder, sidx) => {
      if (idx !== sidx) return shareholder;
      return { ...shareholder, [e.target.name]: e.target.value };
      // this.setState ({
      //   [e.target.name]: e.target.value
      // });
    })
    this.setState({
      ui_items: newShareholders,
      //  totalcosttext : true
    });
  };
  handleAddShareholder = () => {
    this.setState({
      ui_items: this.state.ui_items.concat([{
        item_id: "",
        item_inventory_quantity: '',
        item_batch: '',
        item_batch_expiry: '',
        item_sgst_percentage: '',
        item_cgst_percentage: '',
        item_igst_percentage: '',
        item_total_gst_amount: ''
      }])
    });
  }
  calcCgstAmount =  () => {
    // for (let i = 0; i < this.state.ui_items.length; i++) {
    //   this.state.ui_items[i].item_cgcst_amount = (+this.state.ui_items[0].item_cgst_percentage / 100) * +this.state.item_price
    //   this.state.ui_items[i].item_sgst_amount = (+this.state.ui_items[0].item_sgst_percentage / 100) * +this.state.item_price
    //   this.state.ui_items[i].item_igst_amount = (+this.state.item_igst_percentage / 100) * +this.state.item_price
    //   console.log('cgstamount', this.state.ui_items[i].item_cgcst_amount)
    // }
    let newtotal =  this.state.ui_items.reduce((prev, cur) => (prev + (+cur.item_cgst_percentage)), 0)
    console.log('newtotal',newtotal)
    //  console.log('amount',cgstAmount)
    // await this.setState({
    //   cgstAmount: cgstAmount,
    //   sgstAmount: sgstAmount,
    //   igstAmount: igstAmount
    // })



  }
  taxClick = () => {
    let e = document.getElementById("ddlViewBy");
    let strUser = e.options[e.selectedIndex].text;
    console.clear()
    let newtype = typeof (strUser)
    console.log(newtype)
    let totaltext = parseFloat(strUser) * 2
    let newtaxpercentage = totaltext / 2
    console.log(newtaxpercentage)
    this.formatCurrency(this.calcCgstAmount())
     this.state.ui_items[0].item_cgst_percentage = parseFloat(strUser) * 2
    for(let i=0;i < this.state.ui_items.length;i++) {
    this.state.ui_items[i].item_cgst_percentage = newtaxpercentage
    this.state.ui_items[i].item_sgst_percentage = newtaxpercentage
    console.log(this.state.ui_items[0].item_cgst_percentage, this.state.ui_items[0].item_sgst_percentage)
    }
  

    //  this.formatCurrency(this.calcCgstAmount())
    //  this.formatCurrency(this.calcGstAmount())
  }
  //   handleSubmit = e => {
  //     e.preventDefault();
  //     let body = {

  //       ui_name: this.state.name,

  //       ui_price: this.state.number,
  //       ui_inventory_quantity: this.state.quantity

  //     };
  //     console.log('value here', body)
  //     this.props.postNewProduct(body);
  //      this.props.history.push('/Inventory');
  //   };
  handleSubmit = async e => {
    e.preventDefault();
    // if (this.validator.allValid()) {
    // if (this.state.ui_inventory_quantity > 0) {
    let body = {
      // item_id:this.state.item_id,
      // ui_inventory_quantity: this.state.ui_inventory_quantity,
      // ui_batch: this.state.ui_batch,
      // ui_expiration_date: this.state.ui_expiration_date
      ui_items: this.state.ui_items
    };
    console.log("values here", body);

    //   axios
    //     .put(
    //       `${API_URL}item/add_items/${
    //       this.state.item_id
    //       }`,
    //       body,
    //       (axios.defaults.headers.common["x-auth-token"] = localStorage.getItem(
    //         "jwt"
    //       ))
    //     )
    //     .then(response => {
    //       console.log(response);
    //       // if (response.data.data.kind == 'Item addition') {
    //       //   alert('Product in inventory Added successfully');
    //       //   // this.forceUpdate();
    //       //   // this.handlClick();
    //       //   // window.location.reload();
    //       //   this.props.history.push('/Inventory/1');
    //       // }

    //     })
    //     .catch(error => {
    //       console.log(error);
    //     });
    // } else {
    //   alert("Please Provide valid Quantity");
    // }
    // } else {
    //   this.validator.showMessages();
    //   // rerender to show messages for the first time
    //   this.forceUpdate();
    // }
  };
  render() {
    const { vendors } = this.props;
    return (
      <div>
        <div class="breadcrumb-holder">
          <div class="container-fluid">
            <ul class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="#">Home</a>
              </li>
              <li class="breadcrumb-item active">Forms </li>
            </ul>
          </div>
        </div>
        <br />
        <section class="forms product-forms">
          <div class="container-fluid">
            <div className="row">
              <div className="col-12">
                <div className="card">

                  <div className="card-header d-flex align-items-center">
                    <h4>Add Items in Inventory</h4>

                  </div>
                  <div className="card-body">
                    <p>Fill below details to add Item to Inventory.</p>
                    <form className="custom-content-form">
                      <div className="form-row">
                        <div class="field form-group col-md-6">
                          <div class="form-group row">
                            <label
                              for="inputSubcategory"
                              className="col-sm-3 col-form-label"
                            >
                              Select Vendor
                        </label>
                            <div class="col-sm-9">
                              <select className="form-control" style={{ width: "100%" }}
                                name="name"
                                value={this.state.name}
                                onClick={e => this.vendorclick(e)}
                                onChange={e => this.handleChange(e)}
                              // onChange={(e) => this.setState({item_id: e.target.value})}
                              >
                                <option >Select One </option>
                                {this.props.vendors ? (
                                  this.props.vendors.map(function (item, id) {
                                    return (
                                      <option value={item.id} key={id}>{item.name} </option>
                                    );
                                  }, this)
                                ) : (
                                    <span>Data is loading....</span>
                                  )}
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="form-group col-md-6">
                          <div class="form-group row">
                            <label
                              for="inputSubcategory"
                              className="col-sm-3 col-form-label"
                            >
                              Contact
                        </label>
                            <div class="col-sm-9">
                              <input
                                type="number"
                                class="form-control"
                                id="inputPassword"
                                name="ui_vendor_mobile"
                                value={this.state.ui_vendor_mobile}
                                onChange={e => this.handleChange(e)}
                                disabled
                              />
                            </div>
                          </div>
                        </div>
                        <div class="form-group col-md-6">
                          <div class="form-group row">
                            <label
                              for="inputPassword"
                              class="col-sm-3 col-form-label"
                            >
                              GSTIN
                        </label>
                            <div class="col-sm-9">
                              <input
                                type="text"
                                class="form-control"
                                id="inputPassword"
                                name="ui_vendor_gstin"
                                value={this.state.ui_vendor_gstin}
                                onChange={e => this.handleChange(e)}
                                disabled
                              />

                            </div>
                          </div>
                        </div>
                        <div class="form-group col-md-6">
                          <div class="form-group row">
                            <label
                              for="inputPassword"
                              class="col-sm-3 col-form-label"
                            >
                              Email
                        </label>
                            <div class="col-sm-9">
                              <input
                                type="email"
                                class="form-control"
                                id="inputPassword"
                                name="ui_vendor_email"
                                value={this.state.ui_vendor_email}
                                onChange={e => this.handleChange(e)}
                                disabled
                              />

                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="col-lg-12">
                <div class="card">
                  {/* <div class="card-header d-flex align-items-center">
                    <h4>Add Item in Inventory</h4>
                  </div> */}
                  <div class="card-body">
                    {/* <p>Fill below details to add an Item to Inventory.</p> */}
                    {this.state.ui_items.map((shareholder, idx) => (
                      <form className="custom-content-form">
                        <div className="form-row">
                          <div class="field form-group col-md-4">

                            <select className="form-control" style={{ width: "100%" }} name="item_id"
                              onClick={e => this.siteclick(e)}
                              onChange={e => this.handleChange(e)}
                              
                              >
                              <option >Select One </option>
                              {this.props.product ? (
                                this.props.product.map(function (item, id) {
                                  return (
                                    <option value={item.id} key={id}>{item.name} </option>
                                  );
                                }, this)
                              ) : (
                                  <span>Data is loading....</span>
                                )}
                            </select>
                            <label
                              for="inputSubcategory"
                              className="col-sm-3 col-form-label"
                            >
                              Item Name
</label>


                          </div>
                          {/* <div class="form-group col-md-6">
                            <div class="form-group row">
                              <label
                                for="inputPassword"
                                class="col-sm-3 col-form-label"
                              >
                                Item Name
                        </label>
                              <div class="col-sm-9">
                               
                                <select className="form-control" style={{ width: "100%" }} name="item_id" onChange={e => this.handleChange(e)}>
                                  <option >Select One </option>
                                  {this.props.product ? (
                                    this.props.product.map(function (item, id) {
                                      return (
                                        <option value={item.id} key={id}>{item.name} </option>
                                      );
                                    }, this)
                                  ) : (
                                      <span>Data is loading....</span>
                                    )}
                                </select>

                              </div>
                            </div>
                          </div> */}
                          {/* <div class="field form-group col-4 custom-product-field">

                            <label for="name" class="inp" >
                              <input type="text" id="name" placeholder="&nbsp;" name="item_name" value={shareholder.item_name} onChange={this.change(idx)} />
                              <span class="label">Item Name</span>
                              <span class="border"></span>
                            </label>



                          </div> */}

                          <div class="field form-group col-4 custom-product-field">

                            <label for="quantity" class="inp" name="item_inventory_quantity" value={shareholder.item_inventory_quantity} onChange={this.change(idx)}>
                              <input type="number" id="quantity" placeholder="&nbsp;" />
                              <span class="label">Item Quantity</span>
                              <span class="border"></span>
                            </label>



                          </div>


                          <div class="field form-group col-4 custom-product-field">
                            {/* <p className="text-danger">
                            {this.validator.message(
                              "Quantity",
                              this.state.passitem_inventory_quantityword,
                              "required"
                            )}
                          </p> */}
                            <label for="price" class="inp"
                            >
                              <input type="number" id="price" placeholder="&nbsp;" name="item_price"
                                value={shareholder.item_price}
                                onChange={this.change(idx)} />
                              <span class="label">Item Price</span>
                              <span class="border"></span>
                            </label>
                            {/* <input type="text"
                              name="item_inventory_quantity"
                              value={this.state.item_inventory_quantity}
                              onChange={e => this.handleChange(e)}
                              id="quantity"
                              placeholder="100" />

                            <label for="quantity">Item Quantity</label> */}


                          </div>

                          <div class="field form-group col-6 custom-product-field">
                            {/* <p className="text-danger">
                            {this.validator.message(
                              "Quantity",
                              this.state.passitem_inventory_quantityword,
                              "required"
                            )}
                          </p> */}
                            <label for="batch" class="inp" name="item_batch" value={shareholder.item_batch} onChange={this.change(idx)}>
                              <input type="number" id="batch" placeholder="&nbsp;" />
                              <span class="label">Batch Number</span>
                              <span class="border"></span>
                            </label>
                            {/* <input type="text"
                              name="item_batch"
                              value={this.state.item_batch}
                              onChange={e => this.handleChange(e)}
                              id="batch"
                              placeholder="1256batc" />

                            <label for="batch">Batch Batch Number</label> */}
                            {/* <p className="text-danger">
                              {this.validator.message(
                                "Employee Name",
                                this.state.name,
                                "required|min:3|max:100"
                              )}
                            </p> */}

                          </div>

                          <div class="field form-group col-6 custom-product-field">
                            <label for="expiration" class="inp" name="item_batch_expiry" value={shareholder.item_batch_expiry} onChange={this.change(idx)}>
                              <input type="date" id="expiration" placeholder="&nbsp;" />
                              <span class="label">Batch Expiration Date</span>
                              <span class="border"></span>
                            </label>
                            {/* <input type="text"
                              name="item_batch_expiry"
                              value={this.state.item_batch_expiry}
                              onChange={e => this.handleChange(e)}
                              id="expiry"
                              placeholder="10/12/1997" />

                            <label for="expiry">Batch Expiration Date</label> */}
                            {/* <p className="text-danger">
                              {this.validator.message(
                                "Employee Name",
                                this.state.name,
                                "required|min:3|max:100"
                              )}
                            </p> */}

                          </div>
                          {/* <div class="field form-group col-4 custom-product-field">
                            <label for="cgst" class="inp" >
                              <input type="number" id="cgst" placeholder="&nbsp;" name="item_cgst_percentage" value={this.state.item_cgst_percentage} onChange={e => this.handleChange(e) }/>
                              <span class="label">CGST Percentage </span>
                              <span class="border"></span>
                            </label>
                           

                          </div> */}
                          {/* <div class="field form-group col-4 custom-product-field">
                            <label for="sgst" class="inp" >
                              <input type="number" id="sgst" placeholder="&nbsp;" name="item_sgst_percentage" value={this.state.item_sgst_percentage} onChange={e => this.handleChange(e)}/>
                              <span class="label">SGST Percentage</span>
                              <span class="border"></span>
                            </label>
                           

                          </div> */}
                          <div class="field form-group col-md-6">

                            <select className="form-control" style={{ width: "100%" }}
                              name="item_cgst_percentage"
                              // value={shareholder.item_cgst_percentage}
                              onClick={e => this.taxClick(e)}
                              // onChange={this.change(idx)}
                              id="ddlViewBy"
                            >
                              <option >Choose...</option>
                              <option value='0'>0 + 0</option>
                              <option value='1'>2.500 + 2.500</option>
                              <option value='2'>6 + 6</option>
                              <option value='3'>9 + 9</option>
                              <option value='4'>14 + 14</option>
                              <option value='5'>1.500 + 1.500</option>
                              <option value='6'>0.050 + 0.050</option>
                              <option value='7'>0.125 + 0.125</option>
                              <option value='8'>0.250 + 0.250</option>
                            </select>
                            <label
                              for="inputSubcategory"
                              className="col-sm-3 col-form-label"
                            >
                              CGST + SGST (%)
</label>


                          </div>
                          <div class="field form-group col-4 custom-product-field">
                            <label for="igst" class="inp" >
                              <input type="number" id="igst" placeholder="&nbsp;" name="item_igst_percentage" value={shareholder.item_igst_percentage} onChange={this.change(idx)} />
                              <span class="label">IGST Percentage</span>
                              <span class="border"></span>
                            </label>


                          </div>
                          <div class="field form-group col-4 custom-product-field">
                            <label for="cgst" class="inp" >
                              <input type="text" id="cgst" placeholder="&nbsp;" name="cgstAmount" value={shareholder.cgstAmount} onChange={this.change(idx)} />
                              <span class="label">CGST Amount </span>
                              <span class="border"></span>
                            </label>


                          </div>
                          <div class="field form-group col-4 custom-product-field">
                            <label for="sgst" class="inp" >
                              <input type="text" id="sgst" placeholder="&nbsp;" name="sgstAmount" value={shareholder.sgstAmount} onChange={this.change(idx)} />
                              <span class="label">SGST Amount</span>
                              <span class="border"></span>
                            </label>


                          </div>
                          <div class="field form-group col-4 custom-product-field">
                            <label for="igst" class="inp" >
                              <input type="text" id="igst" placeholder="&nbsp;" name="igstAmount" value={shareholder.igstAmount} onChange={this.change(idx)} />
                              <span class="label">IGST Amount</span>
                              <span class="border"></span>
                            </label>


                          </div>

                          <div class="field form-group col-md-12 custom-product-field">
                            <label for="total" class="inp" >
                              <input type="number" id="total" placeholder="&nbsp;" name="item_total_gst_amount" value={shareholder.item_total_gst_amount} onChange={this.change(idx)} />
                              <span class="label">Total GST Amount</span>
                              <span class="border"></span>
                            </label>
                            {/* <input type="text"
                              name="item_total_gst_amount"
                              value={this.state.item_total_gst_amount}
                              onChange={e => this.handleChange(e)}
                              id="fullname"
                              placeholder="50" />

                            <label for="fullname">Total GST Amount</label> */}
                            {/* <p className="text-danger">
                              {this.validator.message(
                                "Employee Name",
                                this.state.name,
                                "required|min:3|max:100"
                              )}
                            </p> */}

                          </div>

                           <div class="field form-group col-md-6 custom-product-field">
                            <label for="subtotal" class="inp" >
                              <input type="text" id="subtotal" placeholder="&nbsp;" name="item_total_gst_amount" value={shareholder.item_total_gst_amount} onChange={this.change(idx)} />
                              <span class="label">Subtotal Amount</span>
                              <span class="border"></span>
                            </label>
                            {/* <input type="text"
                              name="item_total_gst_amount"
                              value={this.state.item_total_gst_amount}
                              onChange={e => this.handleChange(e)}
                              id="fullname"
                              placeholder="50" />

                            <label for="fullname">Total GST Amount</label> */}
                            {/* <p className="text-danger">
                              {this.validator.message(
                                "Employee Name",
                                this.state.name,
                                "required|min:3|max:100"
                              )}
                            </p> */}

                          </div>

                           <div class="field form-group col-md-6 custom-product-field">
                            <label for="totalamount" class="inp" >
                              <input type="text" id="totalamount" placeholder="&nbsp;" name="item_total_gst_amount" value={shareholder.item_total_gst_amount} onChange={this.change(idx)} />
                              <span class="label">Total Amount</span>
                              <span class="border"></span>
                            </label>
                            {/* <input type="text"
                              name="item_total_gst_amount"
                              value={this.state.item_total_gst_amount}
                              onChange={e => this.handleChange(e)}
                              id="fullname"
                              placeholder="50" />

                            <label for="fullname">Total GST Amount</label> */}
                            {/* <p className="text-danger">
                              {this.validator.message(
                                "Employee Name",
                                this.state.name,
                                "required|min:3|max:100"
                              )}
                            </p> */}

                          </div>

                          {/* <button
                          class="btn btn-primary"
                          onClick={this.handleSubmit.bind(this)}
                          style={{ float: 'right' }}
                        >
                          Submit
                  </button> */}
                        </div>
                        <hr />
                      </form>

                    ))}
                    <button class="btn btn-primary" onClick={this.handleSubmit.bind(this)}
                      style={{ float: 'right' }}>Submit</button>
                    <button type="button" onClick={this.handleAddShareholder} className="btn-info custom-more-btn"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Add More Items</button>
                    <hr />
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <Whatsapp />
              </div>
            </div>
          </div>
        </section>



      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    errors: state.errors,
    product: state.product,
    vendors: state.vendors,
  };
}

export default connect(mapStateToProps, { postNewProduct, fetchTotalProduct, fetchTotalVendors })(AddInventoryItem);