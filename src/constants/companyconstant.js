export const COMPANY_NAME = 'Elipi Software';
export const COMPANY_EMAIL = 'contact@elipi.in';
export const COMPANY_ADDRESS = 'Near Bengali Square';
export const COMPANY_GSTIN = '23BABPG5109C1ZJ';
export const COMPANY_PHONE = '8762028596'